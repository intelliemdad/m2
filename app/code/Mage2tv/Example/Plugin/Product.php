<?php

namespace Mage2tv\Example\Plugin;

use Psr\Log\LoggerInterface;

class Product
{
    private $logger;

    public function __construct(LoggerInterface $loggerInterface)
    {
        $this->logger = $loggerInterface;
    }

    public function afterGetName(\Magento\Catalog\Model\Product $subject, $result) {
        $this->logger->info('working 1');
        $a = 5 + 6;
        $b = 10 + 11;
        $c = $a + $b;
        $this->logger->info('working debugging' . $c);
        return "Apple ". $result; // Adding Apple in product name
    }
}

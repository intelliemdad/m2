<?php
namespace Pulsestorm\TutorialPlugin\Model\Example;
class Plugin
{
    public function beforeGetMessage($subject, $thing='World', $should_lc=false)
    {
        return ['Changing the argument', $should_lc];
    }      

    // public function afterGetMessage($subject, $result)
    // {
    //     echo "Calling " , __METHOD__,"\n";
    //     return 'We are so tired of saying hello';
    // }    
}
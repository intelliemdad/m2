<?php
namespace Pulsestorm\HelloWorldMVVM\Controller\Hello;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;


class World extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;

    protected $baseUrl = "https://keycloak.ddns.net/";

    const AUTHORIZE_URL = "auth/realms/rstore/protocol/openid-connect/auth";
    const ACCESS_TOKEN_URL = "auth/realms/rstore/protocol/openid-connect/token";
    const USER_DATA_URL = "auth/realms/rstore/protocol/openid-connect/userinfo";

    protected $state;
    protected $nonce;
    protected $token;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var CustomerAccountManagement
     */
    private $customerAccountManagement;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Customer $customer,
        AccountManagementInterface $customerAccountManagement,
        CustomerRepositoryInterface $customerRepo,
        CustomerFactory $customerFactory,
        Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->customer = $customer;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerRepo = $customerRepo;
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;

        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    private function getAuthUrl()
    {
        return $this->baseUrl . self::AUTHORIZE_URL;
    }

    private function getAccessTokenUrl()
    {
        return $this->baseUrl . self::ACCESS_TOKEN_URL;
    }

    private function getUserInfoUrl()
    {
        return $this->baseUrl . self::USER_DATA_URL;
    }

    private function post($url, $params, $headers = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $return = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        return $return;
    }

    private function generateRandString() {

        return md5(uniqid(rand(), TRUE));

    }

    private function setNonce() {

        $this->nonce = $this->generateRandString();
        $_SESSION['openid_connect_nonce'] = $this->nonce;

    }

    private function setState() {

        $this->state = $this->generateRandString();
        $_SESSION['openid_connect_state'] = $this->nonce;

    }

    public function execute()
    {

       if( !isset($this->getRequest()->getParams()['code']) ){
           $resultRedirect = $this->resultRedirectFactory->create();
           $resultRedirect->setPath($this->step1());
           return $resultRedirect;
       }else{
           $this->token = $this->step2($this->getRequest()->getParams()['code']);
           if (isset($this->token->id_token)) {

               $userData = $this->step3($this->token);

               $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
               $isEmailNotExists = $this->customerAccountManagement->isEmailAvailable($userData['email'], $websiteId);

               // $customer = $this->customerFactory->setWebsiteId($websiteId)->loadByEmail($userData['email']);

               if (!$isEmailNotExists) {
                   $customerRepo = $this->customerRepo->get($userData['email']);
                   $customer = $this->customerFactory->create()->load($customerRepo->getId());     //get the customer model by id
                   $this->customerSession->setCustomerAsLoggedIn($customer);                       //set the customer to the session
               } else {
                   try {
                       // Instantiate object (this is the most important part)
                       $customer = $this->customerFactory->create();
                       $customer->setWebsiteId($websiteId);

                       // Preparing data for new customer
                       $customer->setEmail($userData["email"]);
                       $customer->setFirstname($userData["given_name"]);
                       $customer->setLastname($userData["family_name"]);
                       $customer->setPassword($this->generateRandString());

                       // Save data
                       $customer->save();
                       $this->customerSession->setCustomerAsLoggedIn($customer);                       //set the customer to the session

//                       $customer = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
//                       $customer->setWebsiteId($websiteId);
//                       $customer->setEmail($email);
//                       $customer->setFirstname($firstName);
//                       $customer->setLastname($lastName);
//                       $customer->setPassword($password);
//
//                       // save customer
//                       $customer->save();
//
//                       $customer->setConfirmation(null);
//                       $customer->save();
//
//                       $customAddress = $objectManager->get('\Magento\Customer\Model\AddressFactory')->create();
//                       $customAddress->setData($address)
//                           ->setCustomerId($customer->getId())
//                           ->setIsDefaultBilling('1')
//                           ->setIsDefaultShipping('1')
//                           ->setSaveInAddressBook('1');
//
//                       // save customer address
//                       $customAddress->save();
//
//                       //reindex customer grid index
//                       $indexerFactory = $objectManager->get('Magento\Indexer\Model\IndexerFactory');
//                       $indexerId = 'customer_grid';
//                       $indexer = $indexerFactory->create();
//                       $indexer->load($indexerId);
//                       $indexer->reindexAll();
//
//                       // Create customer session
//                       $customerSession = $objectManager->create('Magento\Customer\Model\Session');
//                       $customerSession->setCustomerAsLoggedIn($customer);
//
//                       echo 'Customer with email '.$email.' is successfully created.';

                   } catch (Exception $e) {
                       echo $e->getMessage();
                   }
               }

               $resultRedirect = $this->resultRedirectFactory->create();
               $resultRedirect->setPath('customer/account');
               return $resultRedirect;

           }else{
               $resultRedirect = $this->resultRedirectFactory->create();
               $resultRedirect->setPath($this->step1());
               return $resultRedirect;
           }
       }
    }

    public function step1()
    {
        $url = $this->getAuthUrl();

        $this->setNonce();
        $this->setState();

        $params = array(
            'response_type'	=> 'code',
            'redirect_uri'	=> 'http://m234.com/hello_mvvm/hello/world/',
            'client_id'		=> 'digired',
            'nonce'			=> $this->nonce,
            'state'			=> $this->state,
            'scope'			=> 'openid profile email phone'
        );

        $url .= (strpos($url, '?') === false ? '?' : '&') . http_build_query($params, null, '&');
        // session_commit();
        return $url;
    }

    public function step2($code)
    {
        $url = $this->getAccessTokenUrl();

        $params = array(
            'grant_type'	=> 'authorization_code',
            'code'			=> $code,
            'redirect_uri'	=> 'http://m234.com/hello_mvvm/hello/world/',
            'client_id'		=> 'digired',
            'client_secret'	=> 'e9055d9d-4f54-4bc6-8502-28541beb4abb'
        );

        $token = $this->post($url, $params);
        return json_decode($token);
    }

    public function step3($token)
    {
        $url = $this->getUserInfoUrl();

        $headers = array (
            'Authorization: Bearer ' . $token->access_token
        );

        $userInfoObject = json_decode($this->post($url, array(), $headers));

        $user = [];

        $user['sub']  = isset($userInfoObject->sub) ? $userInfoObject->sub : "";
        $user['name'] = isset($userInfoObject->name) ? $userInfoObject->name : "";
        $user['preferred_username']	= isset($userInfoObject->preferred_username) ? $userInfoObject->preferred_username : "";
        $user['given_name']	 = isset($userInfoObject->given_name) ? $userInfoObject->given_name : "";
        $user['family_name'] = isset($userInfoObject->family_name) ? $userInfoObject->family_name : "";
        $user['email'] = isset($userInfoObject->email) ? $userInfoObject->email : "";

        $_SESSION['sub'] = $user['sub'];

        return $user;
    }
}
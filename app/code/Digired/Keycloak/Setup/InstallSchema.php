<?php

namespace Digired\Keycloak\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('keycloak')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Keycloak info ID'
        )->addColumn(
            'base_url',
            Table::TYPE_TEXT,
            null,
            [],
            'Keycloak base url'
        )->addColumn(
            'realm_name',
            Table::TYPE_TEXT,
            null,
            [],
            'Keycloak realm name'
        )->addColumn(
            'client_id',
            Table::TYPE_TEXT,
            null,
            [],
            'Keycloak client Id'
        )->addColumn(
            'client_secret',
            Table::TYPE_TEXT,
            null,
            [],
            'Keycloak client secret'
        )->addColumn(
            'redirect_url',
            Table::TYPE_TEXT,
            null,
            [],
            'Client redirect url'
        );

        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}

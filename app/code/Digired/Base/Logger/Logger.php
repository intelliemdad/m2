<?php
/***********************************************************************************************************************
 * @package    Digired
 * @author     Zahirul Hasan<zhasan@bdipo.com>
 * @copyright  Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Digired\Base\Logger;

/**
 * Class Logger
 * @package Digired\Base\Logger
 */
class Logger extends \Magento\Framework\Logger\Monolog
{
}

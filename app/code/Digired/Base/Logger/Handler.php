<?php
/***********************************************************************************************************************
 * @package    Digired
 * @author     Zahirul Hasan<zhasan@bdipo.com>
 * @copyright  Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Digired\Base\Logger;

/**
 * Class Handler
 *
 * @package Digired\Base\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logger Type
     * @var int
     */
    protected $loggerType = \Monolog\Logger::API;

    /**
     * Handler constructor.
     *
     * @param \Magento\Framework\Filesystem\DriverInterface $filesystem
     * @param string $fileName
     */
    public function __construct(
        \Magento\Framework\Filesystem\DriverInterface $filesystem,
        string $fileName = 'robishop'
    ) {
        $this->fileName = $fileName.'_'.date("Y-m-d").'.log';
        parent::__construct($filesystem, 'var/log/'.$fileName."/");
    }
}

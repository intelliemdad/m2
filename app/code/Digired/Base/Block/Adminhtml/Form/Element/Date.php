<?php
/***********************************************************************************************************************
 * @package    Digired
 * @author     Zahirul Hasan<zhasan@bdipo.com>
 * @copyright  Copyright (c) 2018 - 2020 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Digired\Base\Block\Adminhtml\Form\Element;

/**
 * Class Select
 *
 * @package Digired\Base\Block\Form\Element
 */
class Date extends \Magento\Framework\Data\Form\Element\Date
{
    /**
     * {@inheritdoc}
     */
    public function getElementHtml()
    {
        $this->addClass('admin__control-text  input-text');
        $dateFormat = $this->getDateFormat() ?: $this->getFormat();
        $timeFormat = $this->getTimeFormat();
        if (empty($dateFormat)) {
            throw new \Exception(
                'Output format is not specified. ' .
                'Please specify "format" key in constructor, or set it using setFormat().'
            );
        }

        $calenderConfig = [
            'dateFormat' => $dateFormat,
            'showsTime' => !empty($timeFormat),
            'timeFormat' => $timeFormat,
            'buttonImage' => $this->getImage(),
            'buttonText' => 'Select Date',
            'disabled' => $this->getDisabled()
        ];
        if (!empty($this->getMaxDate())) {
            $calenderConfig['maxDate'] = $this->getMaxDate();
        }

        $dataInit = 'data-mage-init="' . $this->_escape(
            json_encode(['calendar' => $calenderConfig])
        ) . '"';

        $html = sprintf(
            '<input name="%s" id="%s" value="%s" %s %s />',
            $this->getName(),
            $this->getHtmlId(),
            $this->_escape($this->getValue()),
            $this->serialize($this->getHtmlAttributes()),
            $dataInit
        );
        $html .= $this->getAfterElementHtml();
        return $html;
    }
}

<?php
/***********************************************************************************************************************
 * @package    Digired
 * @author     Zahirul Hasan<zhasan@bdipo.com>
 * @copyright  Copyright (c) 2018 - 2020 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Digired\Base\Block\Adminhtml\Form\Element;

/**
 * Class Select
 *
 * @package Digired\Base\Block\Form\Element
 */
class Select extends \Magento\Framework\Data\Form\Element\Select
{
    /**
     * {@inheritdoc}
     */
    public function getElementHtml()
    {
        $html = '';
        if ($this->getBeforeElementHtml()) {
            $html .= '<label class="addbefore" for="' .
                     $this->getHtmlId() .
                     '">' .
                     $this->getBeforeElementHtml() .
                     '</label>';
        }



        $config = [
            'component' => 'Magento_Ui/js/form/element/ui-select',
            'template' => 'Digired_Base/form/elements/select',
            'dataType' => 'text',
            'disableLabel' => true,
            'filterOptions' => true,
            'filterOptionsFocus' => true,
            'multiple' => false,
            'htmlName' => $this->getName(),
            'htmlClass' => $this->getClass(),
            'options' => $this->getValues(),
            'htmlId' => $this->getHtmlId(),
            'value' => $this->getEscapedValue()
        ];

        if (!empty($this->getTitle())) {
            $config["selectedPlaceholders"] =  [
                'defaultPlaceholder' => __($this->getTitle())
            ];
        }
        $html .= '
        <div data-bind="scope: \'' . $this->getHtmlId() . '\'">
            <!-- ko template: getTemplate() --><!-- /ko -->
        </div>
        <script type="text/x-magento-init">
        {
            "*": {
                "Magento_Ui/js/core/app": {
                    "components": {
                        "' . $this->getHtmlId() . '": ' . \Zend\Json\Json::encode($config) . '
                    }
                }
            }
        }
        </script>
        ';


        $html .= '</select>' . "\n";
        if ($this->getAfterElementHtml()) {
            $html .= '<label class="addafter" for="' .
                     $this->getHtmlId() .
                     '">' .
                     "\n{$this->getAfterElementHtml()}\n" .
                     '</label>' .
                     "\n";
        }


        return $html;
    }
}

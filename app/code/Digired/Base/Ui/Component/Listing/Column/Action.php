<?php
/***********************************************************************************************************************
 * @package    Digired
 * @author     Zahirul Hasan<zhasan@bdipo.com>
 * @copyright  Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Digired\Base\Ui\Component\Listing\Column;

/**
 * Class Action
 *
 * @package Digired\Order\Ui\Component\Listing\Order\Cancel\Request\Column
 */
class Action extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $authorization;

    /**
     * Action constructor.
     *
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\AuthorizationInterface $authorization,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
        $this->authorization = $authorization;
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item[$item['id_field_name']])) {
                    if (isset($this->getConfiguration()['links'])
                        && \is_array($this->getConfiguration()['links'])) {
                        $item[$this->getName()] = [];
                        foreach ($this->getConfiguration()['links'] as $linkId => $link) {
                            if (!\is_array($link)
                                || !isset($link['label'])
                                || !isset($link['url'])
                                || (isset($link['acl']) && !$this->isAllowed($link['acl']))
                            ) {
                                continue;
                            }
                            $item[$this->getName()][$linkId] = [
                                'href'  => $this->urlBuilder->getUrl(
                                    $link['url'],
                                    ['id' => $item[$item['id_field_name']]]
                                ),
                                'label' => __($link['label'])
                            ];
                        }
                    }
                }
            }
        }

        return $dataSource;
    }

    /**
     * @param string $aclResource
     *
     * @return bool
     */
    protected function isAllowed(string $aclResource)
    {
        return $this->authorization->isAllowed($aclResource);
    }
}

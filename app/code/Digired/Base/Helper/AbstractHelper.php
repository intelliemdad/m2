<?php
/***********************************************************************************************************************
 * @package    Digired
 * @author     Zahirul Hasan<zhasan@bdipo.com>
 * @copyright  Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Digired\Base\Helper;

/**
 * Class AbstractHelper
 *
 * @package Digired\Base\Helper
 */
abstract class AbstractHelper
{
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Digired\Base\Logger\Logger
     */
    protected $logger;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     *
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * AbstractHelper constructor.
     * @param \Digired\Base\Model\Context $context
     */
    public function __construct(
        \Digired\Base\Model\Context $context
    ) {
        $this->messageManager = $context->getMessageManager();
        $this->logger = $context->getLogger();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->request = $context->getRequest();
        $this->scopeConfig = $context->getScopeConfig();
        $this->response = $context->getResponse();
        $this->storeManager = $context->getStoreManager();
        $this->localeDate = $context->getLocaleDate();
    }

    /**
     * @return \Magento\Framework\Message\ManagerInterface
     */
    public function getMessageManager()
    {
        return $this->messageManager;
    }

    /**
     * @return \Digired\Base\Logger\Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param string|\Magento\Framework\Phrase|\Exception $message
     *
     * @return $this
     */
    public function logDebug($message)
    {
        if (gettype($message) == "string") {
            $message = __($message);
        }

        $this->getLogger()->debug($message);
        return $this;
    }

    /**
     * @param string|\Magento\Framework\Phrase|\Exception $message
     *
     * @return \Magento\Framework\Exception\LocalizedException
     */
    public function logError($message)
    {
        if (gettype($message) == "string") {
            $message = __($message);
        }
        if (!$message instanceof \Exception) {
            $message = new \Magento\Framework\Exception\LocalizedException($message);
        }
        $this->getLogger()->error($message);
        return $message;
    }

    /**
     * @param string|\Magento\Framework\Phrase|\Exception $message
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    public function throwError($message)
    {
        $message = $this->logError($message);
        throw $message;
    }

    /**
     * @param string $field
     * @param int|null $storeId
     *
     * @return mixed
     */
    public function getConfig(string $field, int $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     *
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     *
     * @return \Magento\Framework\UrlInterface
     */
    public function getUrlBuilder()
    {
        return $this->urlBuilder;
    }

    /**
     *
     * @return \Magento\Store\Model\StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore()
    {
        return $this->getStoreManager()->getStore();
    }

    /**
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->getStore()->getName();
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        return $this->getStore()->getCode();
    }

    /**
     *
     * @param bool $fromStore
     * @return string
     */
    public function getStoreUrl(bool $fromStore = true)
    {
        $url = $this->getUrlBuilder()->getBaseUrl();
        if ($fromStore) {
            $url .= http_build_query(
                ['___from_store'=> $this->getStoreCode()],
                '',
                '&amp;'
            );
        }
        return $url;
    }

    /**
     * Encode the mixed $valueToEncode into the JSON format
     *
     * @param mixed $valueToEncode
     * @return string
     */
    public static function jsonEncode($valueToEncode)
    {
        return \Zend\Json\Encoder::encode($valueToEncode);
    }

    /**
     * Decodes the given $encodedValue string which is
     * encoded in the JSON format
     *
     * @param string $encodedValue
     * @return mixed
     */
    public static function jsonDecode(string $encodedValue, bool $asArray = false)
    {
        $type = ($asArray) ? \Zend\Json\Json::TYPE_ARRAY : \Zend\Json\Json::TYPE_OBJECT;
        return \Zend\Json\Decoder::decode($encodedValue, $type);
    }

    /**
     * @param string $time
     * @param \Magento\Store\Api\Data\StoreInterface|null $store
     * @param int $format
     * @param bool $showTime
     * @param null $pattern
     *
     * @return string
     */
    public function formatLocalDateTime(
        string $time,
        \Magento\Store\Api\Data\StoreInterface $store = null,
        int $format = \IntlDateFormatter::MEDIUM,
        bool $showTime = true,
        $pattern = null
    ) {
        if (empty($store)) {
            $store = $this->getStore();
        }
        $showTime = $showTime ? $format : \IntlDateFormatter::NONE;
        $timeZone = $this->localeDate->getConfigTimezone(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store->getCode()
        );
        $dateTime = $this->localeDate->formatDateTime(
            $time,
            $format,
            $showTime,
            null,
            $timeZone,
            $pattern
        );
        return $dateTime;
    }

    /**
     * @param string                                      $time
     * @param \Magento\Store\Api\Data\StoreInterface|null $store
     * @param int                                         $format
     * @return string
     */
    public function formatLocalTime(
        string $time,
        \Magento\Store\Api\Data\StoreInterface $store = null,
        int $format = \IntlDateFormatter::MEDIUM
    ) {
        if (empty($store)) {
            $store = $this->getStore();
        }
        $timeZone = $this->localeDate->getConfigTimezone(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store->getCode()
        );
        $time = $this->localeDate->formatDateTime(
            $time,
            \IntlDateFormatter::NONE,
            $format,
            null,
            $timeZone
        );
        return $time;
    }

    /**
     * @param null $routePath
     * @param null $routeParams
     *
     * @return string
     */
    public function getUrl($routePath = null, $routeParams = null)
    {
        return $this->getUrlBuilder()->getUrl($routePath, $routeParams);
    }
}

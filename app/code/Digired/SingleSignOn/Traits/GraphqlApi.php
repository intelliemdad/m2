<?php

namespace Digired\SingleSignOn\Traits;

trait GraphqlApi{
    private $apiurl;

    private $accesstoken;

    protected $queryJson;

    protected $queryVariables;

    /**
     * @param mixed $accesstoken
     */
    public function setAccesstoken($accesstoken): void
    {
        $this->accesstoken = $accesstoken;
    }

    /**
     * @param mixed $apiurl
     */
    public function setApiurl($apiurl): void
    {
        $this->apiurl = $apiurl;
    }

    /**
     * @param mixed $queryJson
     */
    public function setQueryJson($queryJson): void
    {
        $this->queryJson = $queryJson;
    }

    /**
     * @param mixed $queryVariables
     */
    public function setQueryVariables($queryVariables): void
    {
        $this->queryVariables = $queryVariables;
    }

    private function postQuery(string $query, array $input_data)
    {
        $data = $this->jsonHelper->jsonEncode(['query' => $query, 'variables' => $input_data]);
        $this->curl->addHeader("Content-Type", "application/json");
        $this->curl->addHeader("Content-Length", strlen($data));
        $this->curl->addHeader('Authorization', 'Bearer ' . self::$token);
        $this->curl->post(self::$endPoint, $data);

        $result = $this->curl->getBody();

        if (!empty($result)) {
            $result = $this->jsonHelper->jsonDecode($result);
            $this->logger->info('Response : ' . json_encode($result));
            if (isset($result['errors'])) {
                // error wrapping
            }
        }
    }

    public function hello(){
        return 'hello';
    }
}
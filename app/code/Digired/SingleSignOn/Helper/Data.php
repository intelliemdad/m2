<?php
namespace Digired\SingleSignOn\Helper;

/**
 * Class Data
 * @package Digired\SingleSignOn\Helper
 */
class Data extends \Digired\Base\Helper\AbstractHelper
{
    public const CONFIG_PATH_SSO = "digired_sso/general";
    public const CONFIG_PATH_ENABLED_PROVIDERS = "providers";
    public const CONFIG_PATH_AUTH_URL = "auth_url";
    public const CONFIG_PATH_AUTH_CALLBACK_URL = "callback_url";

    /**
     * @inheritDoc
     */
    public function getConfig(string $field, int $storeId = null)
    {
        return parent::getConfig(self::CONFIG_PATH_SSO . "/" . $field, $storeId);
    }

    /**
     * @return int
     */
    public function getEnabledProdivers()
    {
        return $this->getConfig(self::CONFIG_PATH_ENABLED_PROVIDERS);
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return $this->getConfig(self::CONFIG_PATH_AUTH_URL);
    }

    /**
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->getConfig(self::CONFIG_PATH_AUTH_CALLBACK_URL);
    }

    /**
     * @param string   $code
     * @param string   $field
     * @param int|null $storeId
     *
     * @return mixed
     */
    public function getProviderConfig(string $code, string $field, int $storeId = null)
    {
        return $this->getConfig(
            'providers_config/' . $code . '/' . $field,
            $storeId
        );
    }
}

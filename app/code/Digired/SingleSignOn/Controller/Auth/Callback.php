<?php
namespace Digired\SingleSignOn\Controller\Auth;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Digired\SingleSignOn\Helper\Data;
use Digired\SingleSignOn\Model\Provider;

/**
 * Class Callback
 * @package Digired\SingleSignOn\Controller\Auth
 */
class Callback extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var Data
     */
    private $helper;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Data $helper,
        Provider $provider
    )
    {
        $this->helper = $helper;
        $this->provider = $provider;
        $this->pageFactory = $pageFactory;

        return parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $params['provider'] = $params['provider'] ?? 'rstore';

        $provider = $this->provider->getProviderByCode($params['provider'])
                                   ->getProvider();

        $token = $provider->getAccessToken($params['code']);
        $userData = $provider->getUserInfo($token->access_token);

        $provider->customerLogin($userData);

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('customer/account');
        return $resultRedirect;
    }
}
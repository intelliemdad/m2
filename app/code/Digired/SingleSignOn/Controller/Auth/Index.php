<?php
namespace Digired\SingleSignOn\Controller\Auth;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Digired\SingleSignOn\Helper\Data;
use Digired\SingleSignOn\Model\Provider;

/**
 * Class Index
 * @package Digired\SingleSignOn\Controller\Auth
 */
class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Provider
     */
    private $provider;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Data $helper,
        Provider $provider
    )
    {
        $this->helper = $helper;
        $this->provider = $provider;
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

        $params['provider'] = $params['provider'] ?? 'rstore';

        $authorizationUrl = $this->provider->getProviderByCode($params['provider'])
                                ->getProvider()
                                ->getAuthorizeUrl();

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($authorizationUrl);
        return $resultRedirect;
    }
}
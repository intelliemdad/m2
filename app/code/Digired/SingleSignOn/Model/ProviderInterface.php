<?php
namespace Digired\SingleSignOn\Model;

/**
 * Interface ProviderInterface
 * @package Digired\SingleSignOn\Model
 */
interface ProviderInterface
{
    /**
     * @return \Digired\SingleSignOn\Model\Provider\AbstractProvider
     */
    public function getProvider();

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return string
     */
    public function getLabel(): string;
}

<?php
namespace Digired\SingleSignOn\Model;

/**
 * Class AbstractProvider
 * @package Digired\SingleSignOn\Model
 */
class ProviderList implements \Digired\SingleSignOn\Model\ProviderInterface
{
    /**
     * @var \Digired\SingleSignOn\Model\Provider\AbstractProvider
     */
    protected $provider;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $label;

    /**
     * AbstractProviderList constructor.
     * @param string $code
     * @param string $label
     * @param Provider\AbstractProvider $provider
     */
    public function __construct(
        string $code,
        string $label,
        \Digired\SingleSignOn\Model\Provider\AbstractProvider $provider
    ) {
        $this->code = $code;
        $this->label = $label;
        $this->provider = $provider;
        $this->_construct();
    }

    public function _construct()
    {

        $this->provider->setCode($this->getCode());
    }

    /**
     * @inheritDoc
     */
    public function getCode():string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @InheritDoc
     */
    public function getProvider()
    {
        return $this->provider;
    }
}

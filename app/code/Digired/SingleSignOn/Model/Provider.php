<?php

namespace Digired\SingleSignOn\Model;

/**
 * Class Provider
 * @package Digired\SingleSignOn\Model
 */
class Provider
{
    /**
     * @var \Digired\SingleSignOn\Model\ProviderInterface[]
     */
    protected $providers;

    /**
     * PaymentProvider constructor.
     *
     * @param \Digired\SingleSignOn\Model\ProviderInterface[] $providers
     */
    public function __construct(
        array $providers = []
    ) {
        $this->providers = $providers;
    }

    /**
     * @param string $code
     *
     * @return \Digired\SingleSignOn\Model\ProviderInterface
     */
    public function getProviderByCode(string $code)
    {
        if (!isset($this->providers[$code])) {
            throw new \InvalidArgumentException(
                __('Provider with code "$s" dosen\'t exist', $code)
            );
        }
        return $this->providers[$code];
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $providers = $this->providers;
        $res = [];
        foreach ($providers as $provider) {
            $res[] = [
                'value' => $provider->getCode(),
                'label' => $provider->getLabel(),
            ];
        }

        return $res;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $options = $this->toOptionArray();
        $return = [];

        foreach ($options as $option) {
            $return[$option['value']] = $option['label'];
        }

        return $return;
    }
}

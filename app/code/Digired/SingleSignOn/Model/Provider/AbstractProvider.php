<?php
namespace Digired\SingleSignOn\Model\Provider;

use Digired\SingleSignOn\Helper\Data;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Class AbstractProvider
 * @package Digired\SingleSignOn\Model\Provider
 */
abstract class AbstractProvider
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var \Zend\Http\Headers
     */
    protected $httpHeaders;

    /**
     * @var \Zend\Http\Request
     */
    protected $httpRequest;

    /**
     * @var \Zend\Stdlib\Parameters
     */
    protected $httpParams;

    /**
     * @var \Zend\Http\Client
     */
    protected $httpClient;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @type CustomerFactory
     */
    protected $customerFactory;

    /**
     * @type CustomerSession
     */
    protected $customerSession;

    /**
     * @var AccountManagementInterface
     */
    private $customerAccountManagement;

    /**
     * AbstractProvider constructor.
     * @param Data $helper
     * @param CustomerFactory $customerFactory
     * @param CustomerSession $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     */
    public function __construct(
        Data $helper,
        CustomerFactory $customerFactory,
        CustomerSession $customerSession,
        AccountManagementInterface $customerAccountManagement
    ) {
        $this->helper = $helper;
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->_construct();
    }

    protected function _construct()
    {
        $this->httpHeaders = new \Zend\Http\Headers();
        $this->httpRequest = new \Zend\Http\Request();
        $this->httpParams = new \Zend\Stdlib\Parameters();
        $this->httpClient = new \Zend\Http\Client();
        $options = [
            'adapter'      => \Zend\Http\Client\Adapter\Curl::class,
            'curloptions'  => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 10,
            'timeout'      => 30
        ];
        $this->httpClient->setOptions($options);
        $this->httpRequest->setMethod(\Zend\Http\Request::METHOD_POST);
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setCode(string $code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function setLabel(string $label)
    {
        $this->label = $label;
        return $this;
    }

    public function customerLogin($customerData){
        if($this->isCustomerExist($customerData['email'])){
            $customer = $this->customerFactory->create();
            $customer->setWebsiteId($this->helper->getStore()->getWebsiteId())
                     ->loadByEmail($customerData['email']);
        }else{
            $customer = $this->createCustomer($customerData);
        }

        $this->customerSession->setCustomerAsLoggedIn($customer);
        $this->customerSession->regenerateId();
    }

    public function isCustomerExist($email){
        $websiteId = $this->helper->getStore()->getWebsiteId();
        return !$this->customerAccountManagement->isEmailAvailable($email, $websiteId);
    }

    /**
     * @param array $customerData
     */
    public function createCustomer(array $customerData)
    {
        $customer = $this->customerFactory->create();
        $customer->setFirstname($customerData['firstname'])
            ->setLastname($customerData['lastname'])
            ->setEmail($customerData['email'])
            ->setPassword($this->generateRandString())
            ->setStoreId($this->helper->getStore()->getId())
            ->setWebsiteId($this->helper->getStore()->getWebsiteId())
            ->setCreatedIn($this->helper->getStore()->getName());

        try {
            // If customer exists existing hash will be used by Repository
            $customer->save($customer);
        } catch (Exception $e) {
            $this->helper->logError(
                __(
                    'Error occured when saving customer. Error: %1',
                    $e->getMessage()
                )
            );
        }

        return $customer;
    }

    private function generateRandString()
    {
        return uniqid(rand());
    }

    protected function post($url, $params, $headers = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $return = curl_exec($ch);
        // $info = curl_getinfo($ch);

        if(curl_errno($ch)){
            throw new Exception(curl_error($ch));
        }

        curl_close($ch);

        return $return;
    }

    abstract protected function getIdentityProviderOpenidConnectUrl(string $type);

    /**
     * @return string
     */
    abstract public function getAuthorizeUrl();

    /**
     * @param string $code
     * @return mixed
     */
    abstract public function getAccessToken(string $code);

    /**
     * @param string $accessToken
     * @return array $userInfo
     */
    abstract public function getUserInfo(string $accessToken);
}

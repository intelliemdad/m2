<?php

namespace Digired\SingleSignOn\Model\Provider;

use Digired\SingleSignOn\Traits\GraphqlApi;


/**
 * Class Rstore
 * @package Digired\SingleSignOn\Model\Provider
 */
class Rstore extends AbstractProvider
{
    use GraphqlApi;

    public const CONFIG_PATH_CLIENT_URL = "client_url";
    public const CONFIG_PATH_REALM_NAME = "realm_name";
    public const CONFIG_PATH_CLIENT_ID = "client_id";
    public const CONFIG_PATH_CLIENT_SECRET_KEY = "client_secret";

    /**
     * @var string
     */
    protected $code = "rstore";

    /**
     * @return mixed
     */
    public function getClientUrl()
    {
        return $this->helper->getProviderConfig(
            $this->getCode(),
            self::CONFIG_PATH_CLIENT_URL
        );
    }

    /**
     * @return mixed
     */
    public function getRealmName()
    {
        return $this->helper->getProviderConfig(
            $this->getCode(),
            self::CONFIG_PATH_REALM_NAME
        );
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->helper->getProviderConfig(
            $this->getCode(),
            self::CONFIG_PATH_CLIENT_ID
        );
    }

    /**
     * @return mixed
     */
    public function getClientSecretKey()
    {
        return $this->helper->getProviderConfig(
            $this->getCode(),
            self::CONFIG_PATH_CLIENT_SECRET_KEY
        );
    }

    protected function getIdentityProviderOpenidConnectUrl(string $type){
        $url = $this->getClientUrl() . 'auth/realms/'. $this->getRealmName() .'/protocol/openid-connect/' . $type;
        return $url;
    }

    public function getAuthorizeUrl()
    {
        $openidConnectAuthUrl = $this->getIdentityProviderOpenidConnectUrl('auth');
        
//        $this->setNonce();
//        $this->setState();

        $params = array(
            'response_type'	=> 'code',
            'redirect_uri'	=> $this->helper->getCallbackUrl(),
            'client_id'		=> $this->getClientId(),
//            'nonce'			=> $this->nonce,
//            'state'			=> $this->state,
            'scope'			=> 'openid profile email phone'
        );

        $openidConnectAuthUrl .= (strpos($openidConnectAuthUrl, '?') === false ? '?' : '&') . http_build_query($params, null, '&');
        return $openidConnectAuthUrl;
    }

    public function getAccessToken(string $code)
    {
        $openidConnectTokenUrl = $this->getIdentityProviderOpenidConnectUrl('token');

        $params = array(
            'grant_type'	=> 'authorization_code',
            'code'			=> $code,
            'redirect_uri'	=> $this->helper->getCallbackUrl(),
            'client_id'		=> $this->getClientId(),
            'client_secret'	=> $this->getClientSecretKey(), // 'e9055d9d-4f54-4bc6-8502-28541beb4abb'
            'scope' => 'openid info'
        );

        try{
            $token = $this->post($openidConnectTokenUrl, $params);
            $this->helper->logDebug(
                __(
                    'Sending request to R-store :%1. Req Uri: %2. Response %3',
                    $this->helper->jsonEncode($params),
                    $this->getClientUrl(),
                    $token
                )
            );
        } catch(Exception $e){
            $this->helper->logError(
                __(
                    'Failed to send Curl request for url :%1. \n Request params: %2. \n Error: %3',
                    $openidConnectTokenUrl,
                    $params,
                    $e->getMessage()
                )
            );
        }

        return json_decode($token);
    }

    public function getUserInfo(string $accessToken)
    {
        $openidConnectUserInfoUrl = $this->getIdentityProviderOpenidConnectUrl('userinfo');

        $headers = array (
            'Authorization: Bearer ' . $accessToken
        );

        try{
            $userInfoObject = json_decode($this->post($openidConnectUserInfoUrl, array(), $headers));
        } catch(Exception $e){
            $this->helper->logError(
                __(
                    'Failed to send Curl request for url :%1. \n Request header: %2. \n Error: %3',
                    $openidConnectUserInfoUrl,
                    $headers,
                    $e->getMessage()
                )
            );
        }

        $user = [];

        $user['sub']  = isset($userInfoObject->sub) ? $userInfoObject->sub : "";
        $user['name'] = isset($userInfoObject->name) ? $userInfoObject->name : "";
        $user['preferred_username']	= isset($userInfoObject->preferred_username) ? $userInfoObject->preferred_username : "";
        $user['firstname']	 = isset($userInfoObject->given_name) ? $userInfoObject->given_name : "";
        $user['lastname'] = isset($userInfoObject->family_name) ? $userInfoObject->family_name : "";
        $user['email'] = isset($userInfoObject->email) ? $userInfoObject->email : "";


        $_SESSION['sub'] = $user['sub'];

        return $user;
    }

    public function RstoreOrderLogByGraphqlApi(){
        // $th
    }
}

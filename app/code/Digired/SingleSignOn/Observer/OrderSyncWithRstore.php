<?php

namespace Digired\SingleSignOn\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Digired\SingleSignOn\Model\Provider\Rstore;

class OrderSyncWithRstore implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;
    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    private static $endPoint = 'https://storeapi.ddns.net/graphql/';  // 'http://localhost:4000/graphql'; // can be stored in config later

    private static $token = 'uxxfIG1zORZtVi9MKCuOUaxQ3ZSNdm'; // can be stored in config later

    protected $logger;

    protected $rstore;

    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\HTTP\Client\Curl $curl,
        LoggerInterface $logger,
        Rstore $rstore
    ){
        $this->jsonHelper = $jsonHelper;
        $this->curl = $curl;
        $this->rstore = $rstore;
        $this->logger = $logger;
    }

    public function getOrderData($order){
        $products = [];
        foreach($order->getAllItems() as $item){
            $Product['id']= $item->getProductId();
            $product['name'] = $item->getName();
            $product['sku'] = $item->getSku();
            $product['basePrice'] = $item->getPrice(); // $item->getProduct()->getFinalPrice(); //$item->getBasePrice();
            $product['quantity'] = 1; //$item->getQtyOrdered();
            $product['description'] = 'Dummy Description'; //$item->getProduct()->getShortDescription();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $categories = $item->getProduct()->getCategoryIds();
            $category_names = '';

            foreach($categories as $category){
                $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
                $category_names .= $cat->getName() . ',';
            }

            $category_names = rtrim($category_names, ',');
            $product['category'] = "Q2F0ZWdvcnk6MTU="; //$category_names;
            $products[] = $product;
        }

        $shippingaddress = $order->getShippingAddress();
        $billingaddress = $order->getBillingAddress();

        $shippingAddress = [
            "city" => $shippingaddress->getCity(),
            "streetAddress1" => $shippingaddress->getStreet(),
            "country" => $shippingaddress->getCountryId()
        ];

        $billingAddress = [
            "city" => $billingaddress->getCity(),
            "streetAddress1" => $billingaddress->getStreet(),
            "country" => $billingaddress->getCountryId()
        ];

        $data = [
            "userEmail" => "ag01@rstore.com", // $order->getCustomerEmail(),
            "userPhone" => "01800000001",
            "partnerId" => "digired",
            "partnerOrderId" => $order->getId(),
            "customerNote" => "all new",
            "shippingAddress" => $shippingAddress,
            "billingAddress" => $billingAddress,
            "products" => $products
        ];

        return $data;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            $data = $this->getOrderData($order);
            $query = $this->getQueryJson();
            $result = $this->postQuery($query, $data);

            $this->logger->info('Order output : '. json_encode($result));

            $this->sendCallData();
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }

    public function getQueryJson()
    {
        $query = 'mutation CreateNewOrder(
  $userEmail: String
  $userPhone: String
  $partnerId: String!
  $partnerOrderId: String!
  $customerNote: String
  $shippingAddress: AddressInput
  $billingAddress: AddressInput
  $products: [OrderProductLineInput]!
) {
  createNewOrder(
    input: {
      userEmail: $userEmail
      userPhone: $userPhone
      partnerId: $partnerId
      partnerOrderId: $partnerOrderId
      shippingAddress: $shippingAddress
      billingAddress: $billingAddress
      customerNote: $customerNote
      products: $products
    }
  ) {
    order {
      id
      number
      userEmail
      status
      shippingAddress{
        streetAddress1
        city
      }
      billingAddress{
        streetAddress1
        city
      }
      total {
        net {
          amount
        }
      }
      customerNote
      events {
        orderNumber
        date
        email
      }
      lines {
        productName
        productSku
        quantity
        unitPrice {
          net {
            amount
          }
        }
      }
    }
    orderErrors {
      code
      field
      message
    }
    __typename
  }
}';

        return $query;
    }

    private function postQuery(string $query, array $input_data)
    {
        $data = $this->jsonHelper->jsonEncode(['query' => $query, 'variables' => $input_data]);
        $this->curl->addHeader("Content-Type", "application/json");
        $this->curl->addHeader("Content-Length", strlen($data));
        $this->curl->addHeader('Authorization', 'Bearer ' . self::$token);
        $this->curl->post(self::$endPoint, $data);

        $result = $this->curl->getBody();

        if (!empty($result)) {
            $result = $this->jsonHelper->jsonDecode($result);
            $this->logger->info('Response : ' . json_encode($result));
            if (isset($result['errors'])) {
                // error wrapping
            }
        }

        return $result;
    }
}
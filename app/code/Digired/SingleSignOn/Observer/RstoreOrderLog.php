<?php

namespace Digired\SingleSignOn\Observer;

use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

/**
 * Class RstoreOrderLog
 * @package Digired\SingleSignOn\Observer
 */
class RstoreOrderLog implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;
    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    private static $endPoint = 'https://storeapi.ddns.net/graphql/';  // 'http://localhost:4000/graphql'; // can be stored in config later

    private static $token = 'uxxfIG1zORZtVi9MKCuOUaxQ3ZSNdm'; // can be stored in config later

    protected $logger;

    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\HTTP\Client\Curl $curl,
        LoggerInterface $logger
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->curl = $curl;
        $this->logger = $logger;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            // https://magento.stackexchange.com/questions/268468/get-order-collection-by-order-id-in-magento-2
            $order = $observer->getEvent()->getOrder();
            $orderId = $order->getIncrementId();

            /*get customer details*/
            $customer['lastname'] = $order->getCustomerLastname();
            $customer['firstname'] = $order->getCustomerFirstname();
            $customer['ipaddress'] =$order->getRemoteIp();
            $customer['email'] = $order->getCustomerEmail();
            $customer['id'] = $order->getCustomerId();

            /* get Billing details */
            $billingaddress = $order->getBillingAddress();
            $billinginfo['city'] = $billingaddress->getCity();
            $billinginfo['street'] = $billingaddress->getStreet();
            $billinginfo['postcode'] = $billingaddress->getPostcode();
            $billinginfo['telephone'] = $billingaddress->getTelephone();
            $billinginfo['state_code'] = $billingaddress->getRegionCode();

            /* get shipping details */
            $shippingaddress = $order->getShippingAddress();
            $shippinginfo['city'] = $shippingaddress->getCity();
            $shippinginfo['street'] = $shippingaddress->getStreet();
            $shippinginfo['postcode'] = $shippingaddress->getPostcode();
            $shippinginfo['telephone'] = $shippingaddress->getTelephone();
            $shippinginfo['state_code'] = $shippingaddress->getRegionCode();

            /* get  total */
            $tax_amount = $order->getTaxAmount();
            $total = $order->getGrandTotal();

            $this->logger->info('Emdad #1');
            $this->logger->info('customer :' . json_encode($customer));
            $this->logger->info('billinginfo' . json_encode($billinginfo));
            $this->logger->info('shippinginfo' . json_encode($shippinginfo));

            $this->sendCallData();
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }

    public function sendCallData()
    {
//        $query = <<<MUTATION
//            create_item
//            (
//                board_id: "{$this->getBoardId()()}",
//                item_name: "{$this->getNewStatus()()}",,
//                column_values:
//                "{
//                    \"text9\":\"Hello world\",
//                    \"email\" :{\"email\":\"test@test.com\",\"text\":\"test@test.com\"},
//                    \"dropdown\" :{\"ids\":[6,7,8,9]},
//                    \"numbers\":\"3\",
//                    \"sales_associate\" :{\"ids\":[5]},
//                    \"status\" :{\"label\":\"TEST\"}
//                }"
//            )
//            {
//                id
//            }
//MUTATION;

        $query =
'mutation CreateNewOrder(
  $userEmail: String
  $userPhone: String
  $partnerId: String!
  $partnerOrderId: String!
  $customerNote: String
  $shippingAddress: AddressInput
  $billingAddress: AddressInput
  $products: [OrderProductLineInput]!
) {
  createNewOrder(
    input: {
      userEmail: $userEmail
      userPhone: $userPhone
      partnerId: $partnerId
      partnerOrderId: $partnerOrderId
      shippingAddress: $shippingAddress
      billingAddress: $billingAddress
      customerNote: $customerNote
      products: $products
    }
  ) {
    order {
      id
      number
      userEmail
      status
      shippingAddress{
        streetAddress1
        city
      }
      billingAddress{
        streetAddress1
        city
      }
      total {
        net {
          amount
        }
      }
      customerNote
      events {
        orderNumber
        date
        email
      }
      lines {
        productName
        productSku
        quantity
        unitPrice {
          net {
            amount
          }
        }
      }
    }
    orderErrors {
      code
      field
      message
    }
    __typename
  }
}';

        $input_data = [
            "userEmail" => "ag01@rstore.com",
            "userPhone" => "01800000001",
            "partnerId" => "digired",
            "partnerOrderId" => "87878789",
            "customerNote" => "all new",
            "shippingAddress" => [
                "city" => "Dhaka",
                "streetAddress1" => "BADDA",
                "country" => "BD"
            ],
            "billingAddress" => [
                "city" => "Dhaka",
                "streetAddress1" => "MIRPUR",
                "country" => "BD"
            ],
            "products" => [
                [
                    "name" => "Random name 2",
                    "sku" => "557",
                    "basePrice" => 10,
                    "quantity" => 2,
                    "description" => "my custom description",
                    "category" => "Q2F0ZWdvcnk6MTU="
                ],
                [
                    "name" => "Random Name 2",
                    "sku" => "222",
                    "basePrice" => 4,
                    "quantity" => 5,
                    "description" => "woah its working",
                    "category" => "Q2F0ZWdvcnk6MTU=",
                    "metaData" => [
                        [
                            "key" => "ram",
                            "value" => "8GB"
                        ],
                        [
                            "key" => "brand",
                            "value" => "samsung"
                        ]
                    ]
                ]
            ]
        ];

        $this->postQuery($query, $input_data);
    }

    private function postQuery(string $query, array $input_data)
    {
        $data = $this->jsonHelper->jsonEncode(['query' => $query, 'variables' => $input_data]);
        $this->curl->addHeader("Content-Type", "application/json");
        $this->curl->addHeader("Content-Length", strlen($data));
        $this->curl->addHeader('Authorization', 'Bearer ' . self::$token);
        $this->curl->post(self::$endPoint, $data);

        $result = $this->curl->getBody();

        if (!empty($result)) {
            $result = $this->jsonHelper->jsonDecode($result);
            $this->logger->info('Response : ' . json_encode($result));
            if (isset($result['errors'])) {
                // error wrapping
            }
        }
    }
}